#tutorials in https://pytorch.apachecn.org/docs/1.0/data_loading_tutorial.html
from __future__ import print_function,division
import os
import torch
import pandas as pd   # for csv 
from skimage import io,transform   # graph io & transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transform, utils

import warnings
warnings.filterwarnings("ignore")

plt.ion()     # interactive mode

landmarks_frame = pd.read_csv('data/face')
