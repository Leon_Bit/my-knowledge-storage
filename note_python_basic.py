#----------------USEFULL_FUNCTIONS----------------------------
string.lower() # 变小写字母

a,b = b,a+b
# 上式等价于下面
a = b
b = a+b

type() # 输出数据类型class：int,str,list,tuple等等
# 函数类型可以使用module types中的定义
import types
types.FunctionType			# 自定义的函数
types.BuiltinFunctionType	# 自带的函数
types.LambbdaType			# 匿名函数
types.GeneratorType			# Generator生成器

isinstance(a,(list,tuple))	# 判断a 是否为其中的一种

#------------CLASS-------------------------------------------

class Student(object):			# 类名一般首字母大写 ()中参数的意思为继承object父类，并非变量
	# the first arguement is always self, and not need to assign it when called 传入的参数，self在翻译的时候会自动指向实例
	def __init__(self,name,score,numclass):		# 前后都加__表示这是特殊变量/method
		self.name = name						# 该属性可以从外部访问，赋值或修改
		self.__score = score				# 在名字前面加__，该属性或者函数就变为内部的私有属性，从外部无法访问，会提示没有这个属性，但实际上是变量名被改成了_Student__score (根据python版本不同)
		self._class = numclass			# 在前面加_，为了人为表示这个变量不要从外部访问，
	# 可以通过以下的method来调用或修改属性，这样做的好处是可以在函数中检查输入的参数是否有效再进行赋值
	def get_score(self):
		pass
	def set_score(self,score):
		if 0 <= score <= 100:
			sefl.__score = score
		else:
			raise ValueError('invalid score') 
	def print_score(self):		# Method（方法）: 在class内部定义的函数，直接可以取得实例内部的数据
		print('%s: %s' % (self.name,self.__score))
		
dir(CLASS)	# 用于以list形式列出CLASS内部的所有属性名的字符串

# inheritance继承：子类继承父类的所有属性，并且可以在其基础上做修改，覆盖原有属性
class Animal(object):
	def run():
		pass
	
class Dog(Animal):
	pass

	# 子类是父类，父类不是子类
	a = Animal()
	d = Dog()
	isinstance(d,Dog)		# True
	isinstance(d,Animal)	# True
	isinstance(a,Dog)		# False
	
	
"""
这样的好处是：适用于父类的外部函数同样也对子类适用
此外，Python是一门动态语言，并非适用于父类的函数的输入一定要是该类及其子类，只要这个class有其要调用到的method就可以
"""
# 如：str内部有__len__()方法
len('asd')		# == 'asd'.__len__()  实际上len()函数内部调用了对象的__len__()方法
				# 自定义一个class，如果想用len()函数，就可以在该class 内部定义__len__() method


"""
开闭原则：
	开放拓展：允许子类添加
	封闭修改：不要依赖修改函数来适应子类的属性

"""

#-----------HIGH-ORDER_FUNCTION------------------------------
# 高阶函数：参数可以为另一个函数输入 的函数
	# map()
	ITERATOR = map(FUNCTION,ITERABLE)		# 分别对ITERABLE中的数据执行FUNCTION，返回一个Iterator

	# reduce()
	from functools import reduce
	a = reduce(f,[a,b,c,d,e])			# f = FUNCTION(x1,x2) f must be a 2-arguement function
		# == f((((a,b),c),d),e)
		
	# filter()
	ITERATOR = filter(f,[])		# BOOL = f(a) f返回值为BOOL，filter根据结果保留True的项
	
	# sorted()
	a = sorted([],key=f)			# sorted 按照从小到大的顺序排列，
														# key用于在排序前对[]中的数据做f函数运算，排序完恢复运算前的数据
	
#------------ITERATION--------------------------------------
from collection import Iterable
from collection import Iterator

# iteration - 可用于for...in...
  # 判断类型是否可迭代
    isinstance(VARIABLE, Iterable)
  # 同时迭代 索引index和内容
  		# enumerate([],strat_of_index)   -> (index, list_content), start_of_index指定了第一个标号从哪里开始
     for i,value in enumerate([23,52,54])
      print(i,vaue)
  
	# list generate formula
		[x*x for x in range(1,10) if x != 5]
		[n+m for n in 'abc' for m in 'xyz']
 

# generator 生成大的表需要占用很大的内存，不如直接在用到的时候才生成数据
	# generator 保存的是算法，用于计算下一位
	generator.next()    # 调用算法计算出下一个值，最后一个值之后再调用会有StopIteration Error
	for n in generator  # 或用for取值
		print(n)
	
	# generator 的设计 1- 使用list generate formula, 把[]换成()
	g = (x*x for x in range(1,10) if x != 5)
	
	# generator 的设计 2- 自定义的函数中使用yield keyword，在for里迭代生成
	def fib(max)
		n,a,b = 0,0,1
		while n<max
			yield b				# 函数中为print(b)，这里改成yield 之后，fib()变为generator而不是function,function遇到return返回，而generator遇到yield返回，下次调用.next()时从断点继续
			a,b = b,a+b
			n = n+1
		return 'done'
	f = fib(6)
	next(f)
	f.next()
   
# Iterator： 可以调用next()计算出下一位，用到的时候才计算，可以用于储存无限大的数据（实际上并不是储存数据本身，而是储存获取下一个数据的方法）
	# Iterator vs Iterable
	# Iterable: [],{},'',GENERATOR	可用于for in 语句 
	# Iterator: GENERATOR						可以作用于next()，返回StopIteration Error
	iter(ITERABLE)		# 用于将iterable 变为iterator
	list(ITERATOR)		# 把iterator的内容都计算列出来

		
#----------------STR---------------------------
# str: 
'age: %s, Gender: %s' % (25,'Male')		# == 'age: 25, Gender: Male' 
ord()		# 获取字符编码的整数
chr()		# 输出编码对应的字符
'Hello, {0}, 成绩提升了 {1:.1f}%'.format('小明', 17.125)
# == 'Hello, 小明, 成绩提升了 17.1%'


#----------------LIST----------------------------------
# list: may contain different kinds of data types
mylist = [15,'data',True,[],12]
mylist.append()
mylist.insert(1,A_stuff)
mylist.pop()
mylist.pop(2)
mylist.sort()		# 排序
len(list)


#---------------TUPLE------------------------------------
# tuple: can be regard as an unchangeable list with the same operation with list except assignment
mytuple = ('a',12,[32,5])	# contents inside mytuple[2] can be altered, because mytuple[2] stores an address of the list, the content is at another place
mytuple = (8,)		# tuple with only one element, because (8) == 8

#----------------DICT--------------------------------------
#dict -- dictionary: vs list : quicker but more storage
d = {
    'a':1,
    'b':2,
    'c':3
      }
      
d.key()				#: 'a''b''c'
d.value()			#: 1 2 3
d.items()			#: {...}
d.get('b',errorfb)		#	1  when 'b' is not available in d, print(errorfb)
'd' in d			# False

#---------------------SET------------------------------------
# set: a sequential series, no one repeat another 数学集合
s = set([52,58,6,4,8])		# {4, 6, 8, 52, 58}
s.add(52)									# {4, 6, 8, 52, 58}  no affect
s.remove(52)
s1 | s2
s1 & s2				# OR and AND 

