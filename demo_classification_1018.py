
"""
常用数据库：
https://keras.io/zh/datasets/ 
  CIFAR10：3*32*32, 10 classes pics 50000 for training, 10000 for testing
  CIFAR10：3*32*32, 100 classes pics 50000 for training, 10000 for testing
  
  Imagenet：
  MINIST：uint8*1*28*28, 10 classes pics 60000 for training, 10000 for testing
"""

# torchvision: 加载图片输出为[0, 1]的PILImage
 import torchvision
 
 class torchvision:
  datasets:
		CIFAR10(),
		CIFAR100(),...
	transforms:
		Compose([transforms_functions]),
		ToTensor(),													# convert PIL Image or numpy.ndarray into tensor
		
	
  
 
 torch.utils.data.DataLoader(dataset,batch_size=0,shuffle=False,num_workers=0)
 
 训练分类器的步骤：
  1.  通过torchvision加载CIFAR10里的训练和测试数据集,然后做标准化[-1, 1]
	#-----------------------------
		import torch
		import torchvision
		import torchvision.transforms as transforms
			# 搭建转换器，这里先转换为tensor类型之后做标准化
		transform = transforms.Compose(
				[transforms.ToTensor(),
				transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))]
		)
			# 加载dataset到root，并按照transform 函数对数据进行transform
		trainset = torchvision.dataset.CIFAR10(root="./data",train = True, download = True, transform = transform)
			# 数据加载到了位置中，通过DataLoader在训练/测试的时候读出
				# shuffle=Bool: 随机/顺序读取；
				# batch_size：每次读取sample数量；
				# sampler，batch_sampler=function：从dataset中取样本的策略；
		trainloader = torch.utils.data.DataLoader(trainset,batch_size=4,shuffle=False,num_workers=2)	
			<a href="https://pytorch.org/docs/stable/data.html#torch.utils.data.DataLoader">DataLoader Ref</a> 

		testset = trochvision.dataset.CIFAR10(root='./data',train=False,download=True,transform=transform)
		testloader = torch.utils.data.DataLoader(testset,batch_size=4,shuffle=False,num_workers=2)

		# generator iterator
		dataiter = iter(trainloader)
		images, labels = dataiter.next()		# images, labels = [4ELEMENTS]

		# display images:
		import torchivision
		import matplotlib.pyplot as plt
		import numpy as np
		def imshow(img):
			img = img/2 +0.5	# anti-normalizing
			npimg = img.numpy()
			plt.imshow(np.transpose(npimg,(1,2,0)))
			plt.imshow()

		imshow(torchvision.utils.make_grid(images))
		print(' '.join('%5s' % classes[labels[j]] for j in range(4)))


2.  define the CNN Model
	# refer to mynote https://github.com/Leon-Z12/my-knowledge-storage/blob/master/demo_model_1_1012
		import torch.nn as nn
		import torch.nn.functional as F
		class Net(nn.Module):
			def __init__(self):
				super(Net,self).__init__()
				self.conv1	= nn.Conv2d(3,6,5)
				self.pool	= nn.MaxPool2d(2,2)
				self.conv2	= nn.Conv2d(6,16,5)
				self.fc1	= nn.Linear(16*5*5,120)
				self.fc2	= nn.Linear(120,84)
				self.fc3	= nn.Linear(84,10)

			def forward(self,x):
				x = self.pool(F.relu(self.conv1(x)))
				x = self.pool(F.relu(self.conv2(x)))
				x = x.view(-1, 16*5*5)
				x = F.relu(self.fc1(x))
				x = F.relu(self.fc2(x))
				x = self.fc3(x)
				return x

		net = Net()
						
  3.  define the loss function
	# use momentum here
		import torch.optim as optim

		criterion = nn.CrossEntropyLoss()	# criterion(output_list, Label_list) = -log(SOFTMAX)
				# formular of Cross Entropy Loss function can be found here https://pytorch.org/docs/stable/nn.html#crossentropyloss

		# perameter updating
		optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
			
  4.  train the NN with training data set
			
		for epoch in range(2):
			running_loss = 0.0
			for i,data in enumerate(trainloader,0):
				inputs,labels = data		# get an input image and its label
				optimizer.zero_grad()

				outputs = net(inputs)
				loss = criterion(outputs, labels)
				loss.backward()
				optimizer.step()

				running_loss += loss.item()		# loss is a tensor, loss.item() gets the value
				if i % 2000 == 1999:
					print('[%d, %5d] loss: %.3f' % (epoch+1, i+1, running_loss/2000))		# aerage loss
					running_loss = 0.0

		print('Finish Training')
	
  5.  test the NN with testing data set
	# mini-batch 输出标签
		# 取用于测试的图像
		dataiter = iter(testloader)
		images,labels = dataiter.next()
		# 先把用于测试的图像打印出来并显示对应的labels
		imshow(torchvision.utils.make_grid(images))
		print('GroundTruth: '+' '.join('%5s' % classes[labels[j]] for j in range(4)))

		output = net(images)

		_,predicted = torch.max(output,1)
		print('Predict: '+' '.join('%5s' % classes[predicted[j]] for j in range(4)))

	# the whole dataset 输出百分比
		# 总的准确度
		correct = 0
		total = 0
		with torch.no_grad():		# disable the gradient computing within the WITH block
			for data in testloader:
				images, labels = data
				outputs = net(images)
				_,predict = torch.max(outputs,1)
				
				total += labels.size(0)
				correct += (predict == labels).sum().item()
		
		print('Accuracy of the network on 10000 test images: %d %%' % (100*correct/total))

		# 每个类别的准确度
		class_total = list(0. for i in range(10))		# 0. == 0.0
		class_correct = list(0. for i in range(10))
		with torch.no_grad():
			for data in testloader:
				images,labels = data
				outputs = net(images)
				_,predict = torch.max(outputs,1)
				c = (predict == labels).squeeze()
				for i in range(4):
					label = labels[i]
					class_correct[label] += c[i].item()
					class_total[label] += 1
		
		for i in range(10):
			print('Accuracy of %5s: %2d %%' % (classes[i],100*class_correct[i]/class_total[i]))
		
		# combine together
		correct = 0
		total = 0
		class_total = list(0. for i in range(10))		# 0. == 0.0
		class_correct = list(0. for i in range(10))
		with torch.no_grad():
			for data in testloader:
				images,labels = data
				outputs = net(images)
				_,predict = torch.max(outputs,1)
				
				total += labels.size(0)
				correct += (predict == labels).sum().item()
				c = (predict == labels).squeeze()
				for i in range(4):
					label = labels[i]
					class_correct[label] += c[i].item()
					class_total[label] += 1
		
		print('Accuracy of the network on 10000 test images: %d %%' % (100*correct/total))
		for i in range(10):
			print('Accuracy of %5s: %2d %%' % (classes[i],100*class_correct[i]/class_total[i]))
		
		
		
PS: 使用GPU加速
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print(device)
# 将模型转换为CUDA tensor，输入和target都放到GPU
net.to(device)
inputs,labels = inputs.to(device),labels.to(device)
# 调用所有GPU的方法： https://pytorch.org/tutorials/beginner/blitz/data_parallel_tutorial.html 

		
