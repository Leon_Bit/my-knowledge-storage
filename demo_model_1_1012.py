import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        # 输入图像channel：1；输出channel：6；5x5卷积核
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        # an affine operation: y = Wx + b
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # 2x2 Max pooling
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        # 如果是方阵,则可以只使用一个数字进行定义
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # 除去批处理维度的其他所有维度
        num_features = 1
        for s in size:
            num_features *= s
        return num_features

# setup a CNN model
net = Net()
print(net)

# input something
input = torch.randn(1,1,32,32)
out = net(input)

# see its parameters
params = list(net.parameters())
print(params)
print(params[1].size())  # conv1's .weight

# calculate loss function
criterion = nn.MSELoss()    # any loss function, more in <https://pytorch.org/docs/stable/nn.html#loss-functions>
  # say the target output result is tensor target
loss = criterion(out,target)
 

# backward propagation
net.zero_grad()   # clear original gradients to avoid overlay
loss.backward(retain_graph = True)   	# set loss.requires_grad = True to enable the tracking of how it is figured out(default: ... =Ture)
																			# retain_graph arguement is used in the first time doing backward propagation
																			# to store the propagation graph in order to backward the second time
																			
# update parameters 
  	# manually
	learning_rate = 0.01
	for f in net.parameters():
  f.data.sub_(f.grad.data * learning_rate)
  
  	# with torch.optim library
	import toch.optim as optim
	optimizer = optim.SGD(net.parameters(), lr=0.01)
	optimizer.step()


#---------------more simple coding after define the model----------------------------
import torch.optim as optim

optimizer = optim.SGD(net.parameters(),lr=0.01)

optimizer.zero_grad()
output = net(input)
loss = criterion(output,target)
loss.backward()
optimizer.step()






